Elab V1.0

eLab is a computer-assisted student learning environment. eLab is a digitized platform that supports Flipped Classroom Model. 
This helps in independent practice work for the students even after the class hours. 
Within the classroom teacher extends his/her support and help in solving the problems. 
Flipping a classroom allows teachers to help busy and struggling students towards programming activities. 
This platform increases student-teacher and student- student interaction during class hours. 
Also gives teachers the time to provide more individualized differentiation, alleviates classroom management issues.


System Requirement:

	- Ubuntu 14.04
	- Apache2
	- mysql
	- php5.4
	- g++

Pre settings :
	- change apache port rather than PORT 80
	- Jail system run in PORT 80
The following PHP extensions are required or recommended (some, e.g. iconv, ctype and tokenizer are now included in PHP by default). Others will need to be installed or selected.

    - The iconv extension is required.
    - The mbstring extension is recommended.
    - The curl extension is required (required for networking and web services).
    - The openssl extension is recommended (required for networking and web services).
    - The tokenizer extension is recommended.
    - The xmlrpc extension is recommended (required for networking and web services).
    - The soap extension is recommended (required for web services).
    - The ctype extension is required.
    - The zip extension is required.
    - The gd extension is recommended (required for manipulating images).
    - The simplexml extension is required.
    - The spl extension is required.
    - The pcre extension is required.
    - The dom extension is required.
    - The xml extension is required.
    - The intl extension is recommended.
    - The json extension is required.
   the appropriate extension for your chosen database is required

Other PHP extensions may be required to support optional functionality, especially external authentication  (e.g. LDAP extension for LDAP authentication and the sockets extension for Chat server).

Installation Steps:

	- Unzip “vpl-jail-system-2.1.1” run “install-vpl-sh” (./install-vpl-sh)
	- Unzip “Program, elabV1” in apache folder
	- Copy “codehere_data” inside eLabV1 and paste in “/var/”
	- Import “eLabV2.sql” to mysql database “codehere”

Configuring Steps:

In apache folder

	- program->config.php (mysql username and password, deployed location)
	- codehere->config.php (mysql username and password, deployed location)
	- code-here->config->database.php (mysql username and password)

Default Username and Password:

	- Username: admin
	- Password: Hari18@Naveen 

Server Maintanace:

Apache2 server

	- service apache2 start
	- service apache2 stop
	- service apache2 restart
	- service apache2 status

Vpl-jail server

	- service vpl-jail-system start
	- service vpl-jail-system stop
	- service vpl-jail-system restart
	- service vpl-jail-system status

### Developer Naveen Kumar S###

phone : +91 90476 90104

email : vssnaveenkumar@gmail.com

linkedin : https://in.linkedin.com/in/vssnaveenkumar


